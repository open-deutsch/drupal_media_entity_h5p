<?php

namespace Drupal\ld_media_entity_h5p\Plugin\media\Source;

use Drupal\media\MediaSourceBase;

/**
 * H5P entity media source.
 *
 * @MediaSource(
 *   id = "h5p",
 *   label = @Translation("H5P"),
 *   description = @Translation("Implements H5P content as media source."),
 *   allowed_field_types = {"h5p"},
 *   default_thumbnail_filename = "iframe.png"
 * )
 */
class H5P extends MediaSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes () {
    return [
    ];
  }

}
